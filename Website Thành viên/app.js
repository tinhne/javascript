document.addEventListener('DOMContentLoaded', function() {
  const memberForm = document.getElementById('member-form');
  const memberList = document.getElementById('memberList');
  let members = [];
  let editingIndex = -1;

  // Xử lý sự kiện gửi form
  memberForm.addEventListener('submit', function(event) {
      event.preventDefault();
      
      const fullName = document.getElementById('fullName').value;
      const birthdate = document.getElementById('birthdate').value;
      const studentID = document.getElementById('studentID').value;
      const className = document.getElementById('class').value;
      
      if (editingIndex === -1) {
          // Thêm thành viên mới
          members.push({ fullName, birthdate, studentID, className });
      } else {
          // Cập nhật thành viên
          members[editingIndex] = { fullName, birthdate, studentID, className };
          editingIndex = -1;
          document.getElementById('form-button').textContent = 'Thêm thành viên';
      }

      // Xóa form sau khi gửi
      memberForm.reset();
      updateMemberList();
  });

  // Hàm cập nhật danh sách thành viên
  function updateMemberList() {
      memberList.innerHTML = '';
      members.forEach((member, index) => {
          const li = document.createElement('li');
          li.innerHTML = `
              <span>${member.fullName} - ${member.birthdate} - Mã: ${member.studentID} - Lớp: ${member.className}</span>
              <button onclick="editMember(${index})">Sửa</button>
              <button onclick="deleteMember(${index})">Xóa</button>
          `;
          memberList.appendChild(li);
      });
  }

  // Hàm sửa thành viên
  window.editMember = function(index) {
      const member = members[index];
      document.getElementById('fullName').value = member.fullName;
      document.getElementById('birthdate').value = member.birthdate;
      document.getElementById('studentID').value = member.studentID;
      document.getElementById('class').value = member.className;
      
      editingIndex = index;
      document.getElementById('form-button').textContent = 'Cập nhật thành viên';
  }

  // Hàm xóa thành viên
  window.deleteMember = function(index) {
      members.splice(index, 1);
      updateMemberList();
  }
});
